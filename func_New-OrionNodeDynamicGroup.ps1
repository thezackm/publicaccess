﻿#region Top of Script

#requires -version 2
<#
.SYNOPSIS 
    Creates Groups used for Dependencies

.DESCRIPTION 
    Requires CMDB site_id and sys_class_names be populated in Nodes Custom Properties

.NOTES
    Version:        1.0
    Author:         Zack Mutchler
    Creation Date:  June 27, 2018
    Purpose/Change: Initial Script development.  
#>

#endregion

#####-----------------------------------------------------------------------------------------#####

#region Function Set-SwisConnection

Function Set-SwisConnection {
    
    Param(
        [Parameter( Mandatory = $true, HelpMessage = "What SolarWinds server are you connecting to (Hostname or IP)?" ) ] [string ] $SolarWindsServer,
        [Parameter( Mandatory = $true, HelpMessage = "Do you want to use the credentials from PowerShell (Trusted), or a new login (Explicit)?" ) ] [ ValidateSet( 'Trusted', 'Explicit' ) ] [string ] $ConnectionType
    )

    # Import the SolarWinds PowerShell Module
    Import-Module SwisPowerShell

    # Connect to SWIS
    IF ( $ConnectionType -eq 'Trusted'  ) {

        $swis = Connect-Swis -Trusted -Hostname $SolarWindsServer
    
    }

    ELSE {
    
        $creds = Get-Credential -Message "Please provide a Domain or Local Login for SolarWinds"

        $swis = Connect-Swis -Credential $creds -Hostname $SolarWindsServer

    }

RETURN $swis

}

#endregion Function Set-SwisConnection

#####-----------------------------------------------------------------------------------------#####

#region Function New-OrionNodeDynamicGroup

Function New-OrionNodeDynamicGroup {

    Param(
        
        [ Parameter( Mandatory = $true, HelpMessage = "Value from the Set-SwisConnection Function" ) ] [ object ] $SwisConnection,
        [ Parameter( Mandatory = $true, HelpMessage = "Array of Site IDs" ) ] [ string[] ] $SiteIDs

        )

    Foreach ( $s in $SiteIDs ) {
        
        # Create Parent and Child Group Names
        $parentGroup = "p_$s"
        $childGroup = "c_$s"

        # Get the DateTime for the group descriptions
        $now = Get-Date

        # Create Parent Member Filter
        $parentMembers = @(
            @{ Name = "$parentGroup"; Definition = "filter:/Orion.Nodes[CustomProperties.n_sn_site_id='$s' AND CustomProperties.n_sn_sys_class_name='IP Router' AND CustomProperties.OwnerGroup='NETWORK']" }
        )

        # Create Child Member Filter
        $childMembers = @(
            @{ Name = "$childGroup"; Definition = "filter:/Orion.Nodes[CustomProperties.n_sn_site_id='$s' AND CustomProperties.n_sn_sys_class_name!='IP Router' AND CustomProperties.OwnerGroup='NETWORK']" }
        )

        # Show the results in the console
        Write-Host "$( $s ) PARENT FILTER: $( $parentMembers.('Name') )`n    $( $parentMembers.('Definition') ) `n$( $s ) CHILD FILTER: $( $childMembers.('Name') )`n    $( $childMembers.('Definition') )`n`n" -ForegroundColor Yellow
    
        # Create Parent Group with Members
        Write-Host "Creating $( $parentGroup )..." -ForegroundColor Green

        $parentID = (Invoke-SwisVerb $SwisConnection "Orion.Container" "CreateContainer" @(
            # group name
            $parentGroup,

            # owner, must be 'Core'
            "Core",

            # refresh frequency
            60,

            # Status rollup mode:
            # 0 = Mixed status shows warning
            # 1 = Show worst status
            # 2 = Show best status
            0,

            # group description
            "Initial Group Created via PowerShell Script on $now",

            # polling enabled/disabled = true/false (in lowercase)
            "true",

            # group members
            ([xml]@(
               "<ArrayOfMemberDefinitionInfo xmlns='http://schemas.solarwinds.com/2008/Orion'>",
               [string]($parentMembers |% {
                 "<MemberDefinitionInfo><Name>$($_.Name)</Name><Definition>$($_.Definition)</Definition></MemberDefinitionInfo>"
                 }
               ),
               "</ArrayOfMemberDefinitionInfo>"
            )).DocumentElement
        )).InnerText

        $parentID

        # Create Child Group with Members
        Write-Host "Creating $( $childGroup )..." -ForegroundColor Green
        
        $childID = (Invoke-SwisVerb $SwisConnection "Orion.Container" "CreateContainer" @(
            # group name
            $childGroup,

            # owner, must be 'Core'
            "Core",

            # refresh frequency
            60,

            # Status rollup mode:
            # 0 = Mixed status shows warning
            # 1 = Show worst status
            # 2 = Show best status
            0,

            # group description
            "Initial Group Created via PowerShell Script on $now",

            # polling enabled/disabled = true/false (in lowercase)
            "true",

            # group members
            ([xml]@(
               "<ArrayOfMemberDefinitionInfo xmlns='http://schemas.solarwinds.com/2008/Orion'>",
               [string]($childMembers |% {
                 "<MemberDefinitionInfo><Name>$($_.Name)</Name><Definition>$($_.Definition)</Definition></MemberDefinitionInfo>"
                 }
               ),
               "</ArrayOfMemberDefinitionInfo>"
            )).DocumentElement
        )).InnerText
    
        $childID

    }

}

#endregion Function New-OrionNodeDynamicGroup

#####-----------------------------------------------------------------------------------------#####

#region Function Set-SubGroupMembers

Function Set-SubGroupMembers {

    Param(
        
        [ Parameter( Mandatory = $true, HelpMessage = "Value from the Set-SwisConnection Function" ) ] [ object ] $SwisConnection,
        [ Parameter( Mandatory = $true, HelpMessage = "Array of Group URIs" ) ] [ string[] ] $GroupURIs,
        [ Parameter( Mandatory = $true, HelpMessage = "Master Group ID" ) ] [ int ] $MasterGroup

        )
    
    Foreach( $uri in $GroupURIs ) {
        Invoke-SwisVerb $swis "Orion.Container" "AddDefinition" @(
	        # group ID
	        $MasterGroup,

	        # group member to add
	        ([xml]"
		        <MemberDefinitionInfo xmlns='http://schemas.solarwinds.com/2008/Orion'>
		            <Name></Name>
		            <Definition>$uri</Definition>
	            </MemberDefinitionInfo>"
	        ).DocumentElement
        ) | Out-Null

        Write-Host "Adding member to Master Group: $( $uri )" -ForegroundColor Yellow

    }

}

#endregion Function Set-SubGroupMembers

#####-----------------------------------------------------------------------------------------#####

#region Variables

$poller = "WPIL0219SWWEB01"

$siteIDQuery = @"
SELECT DISTINCT 
	p.n_sn_site_id
FROM (
	SELECT DISTINCT 
		n_sn_site_id
		,COUNT(1) AS [Qty] 
	FROM Orion.NodesCustomProperties 
	WHERE n_sn_sys_class_name = 'IP Router' 
	AND n_sn_sys_class_name IS NOT NULL 
	AND OwnerGroup = 'NETWORK' 
	AND n_sn_site_id NOT IN ( 'CHN42', 'CHND0', 'IND01', 'IND02', 'IND09', 'OH001', 'IL021', 'OH067' ) 
	GROUP BY n_sn_site_id 
	HAVING COUNT(1) > 0
) p
JOIN (
	SELECT DISTINCT 
		n_sn_site_id
		,COUNT(1) AS [Qty] 
	FROM Orion.NodesCustomProperties 
	WHERE n_sn_sys_class_name <> 'IP Router' 
	AND n_sn_sys_class_name IS NOT NULL 
	AND OwnerGroup = 'NETWORK' 
	AND n_sn_site_id NOT IN ( 'CHN42', 'CHND0', 'IND01', 'IND02', 'IND09', 'OH001', 'IL021', 'OH067' ) 
	GROUP BY n_sn_site_id 
	HAVING COUNT(1) > 0
) c ON c.n_sn_site_id = p.n_sn_site_id
"@

$parentURIQuery = @"
SELECT URI FROM Orion.Container WHERE Name LIKE 'p[_]%'
"@

$childURIQuery = @"
SELECT URI FROM Orion.Container WHERE Name LIKE 'c[_]%'
"@

$masterGroupQuery = @"
SELECT ContainerID FROM Orion.Container WHERE Name = 'Dependencies'
"@

#endregion Variables

#####-----------------------------------------------------------------------------------------#####

#region Execution

    $swis = Set-SwisConnection -SolarWindsServer $poller -ConnectionType Trusted

# Build all of the groups for the Site ID array
    #$siteArray = Get-SwisData -SwisConnection $swis -Query $siteIDQuery

    $siteArray = @( 'AK003', 'AL003', 'AL016', 'AL019' )

    Write-Host "SITE ARRAYS = $( $siteArray )" -ForegroundColor Gray

    New-OrionNodeDynamicGroup -SwisConnection $swis -SiteIDs $siteArray

# Put the Parent and Child Groups into the Master Group
    $masterGroup = Get-SwisData -SwisConnection $swis -Query $masterGroupQuery

    $parentArray = Get-SwisData -SwisConnection $swis -Query $parentURIQuery

    $childArray = Get-SwisData -SwisConnection $swis -Query $childURIQuery

    Set-SubGroupMembers -SwisConnection $swis -GroupURIs $parentArray -MasterGroup $masterGroup

    Set-SubGroupMembers -SwisConnection $swis -GroupURIs $childArray -MasterGroup $masterGroup

# Verification

    Write-Host "`n`nLooks good, let's go check it out..." -ForegroundColor Green

    #Start-Process "http://monitoring.cardinalhealth.net/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:496"

#endregion Execution

#####-----------------------------------------------------------------------------------------#####