﻿#region Top of Script

#requires -version 2
<#
.SYNOPSIS 
    SNMP Testing for all Network Devices in PROD SolarWinds

.DESCRIPTION 
    Requires the SNMP module from the PowerShell Gallery https://www.powershellgallery.com/packages/SNMP/1.0.0.1

.NOTES
    Version:        1.0
    Author:         Zack Mutchler
    Creation Date:  October 4, 2018
    Purpose/Change: Initial Script development.  
#>

#endregion

#####-----------------------------------------------------------------------------------------#####

#region Functions

# Create a function to connect to the SolarWinds Information Service (SWIS)
Function Set-SwisConnection {
    
    Param(
        [Parameter( Mandatory = $true, HelpMessage = "What SolarWinds server are you connecting to (Hostname or IP)?" ) ] [string ] $SolarWindsServer,
        [Parameter( Mandatory = $true, HelpMessage = "Do you want to use the credentials from PowerShell (Trusted), or a new login (Explicit)?" ) ] [ ValidateSet( 'Trusted', 'Explicit' ) ] [string ] $ConnectionType
    )

    # Import the SolarWinds PowerShell Module
    Import-Module SwisPowerShell

    # Connect to SWIS
    IF ( $ConnectionType -eq 'Trusted'  ) {

        $swis = Connect-Swis -Trusted -Hostname $SolarWindsServer
    
    }

    ELSE {
    
        $creds = Get-Credential -Message "Please provide a Domain or Local Login for SolarWinds"

        $swis = Connect-Swis -Credential $creds -Hostname $SolarWindsServer

    }

RETURN $swis

}

#endregion Functions

#####-----------------------------------------------------------------------------------------#####

#region Variables

# Set the SolarWinds server to connect to
$hostname = Read-Host -Prompt "What SolarWinds Server are you connecting to?"

# Create SWQL Queries for use later
$query = @"
SELECT TOP 10
n.NodeID
,n.IP_Address
,n.Community
,n.Caption
FROM Orion.Nodes n 
WHERE n.Status = 1
AND n.ObjectSubType = 'SNMP'
AND n.CustomProperties.OwnerGroup = 'NETWORK'
"@

# Set the OID to query (SysName)
$oid = '1.3.6.1.2.1.1.5.0'

#endregion Variables

#####-----------------------------------------------------------------------------------------#####

#region Execution

# Connect to SWIS
$swis = Set-SwisConnection -SolarWindsServer $hostname -ConnectionType Trusted

# Query SWIS and set results to variable
$nodes = Get-SwisData -SwisConnection $swis -Query $query


$results = @()
Foreach( $n in $nodes ) {

    $snmp = Get-SnmpData -IP $n.IP_Address -OID $oid -Community $n.Community -ErrorAction SilentlyContinue

    If ( !( $snmp.Data ) ) { $test = 'Fail' }
    Else { $test = 'Pass' }

    $item = New-Object -TypeName psobject
    $item | Add-Member -MemberType NoteProperty -Name 'nodeID' -Value $n.NodeID
    $item | Add-Member -MemberType NoteProperty -Name 'deviceName' -Value $n.Caption
    $item | Add-Member -MemberType NoteProperty -Name 'ipAddress' -Value $n.IP_Address
    $item | Add-Member -MemberType NoteProperty -Name 'result' -Value $test

    $results += $item 

}

$results | Format-Table -AutoSize

#endregion Execution